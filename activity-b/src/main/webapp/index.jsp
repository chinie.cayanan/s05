<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SERVLET JOB FINDER</title>

<style>
	div {
		margin-top: 5px;
		margin-bottom: 5px
		}
</style>
	
</head>
<body>

	<h1>Welcome to Servlet Job Finder</h1>

	<form method="post" action="register">
	
		<div>
			<label for="firstname">First Name: </label>
			<input type="text" name="firstname" required> 
		</div>
		
		<div>
			<label for="lastname">Last Name: </label>
			<input type="text" name="lastname" required> 
		</div>
		
		<div>
			<label for="phone">Phone</label>
			<input type="tel" name="phone" required> 
		</div>
		
		<div>
			<label for="email">Email</label>
			<input type="email" name="email" required> 
		</div>
		
		<fieldset>
		
			<legend>How did you discover the app?</legend>
			
			<input type="radio" id="friends" name="discover" required value="friends">
			<label for="friends">Friends</label>
			<br>
			
			<input type="radio" id="socialmedia" name="discover" required value="socialmedia">
			<label for="socialmedia">Social Media</label>
			<br>
			
			<input type="radio" id="others" name="discover" required value="others">
			<label for="others">Others</label>
			<br>
			
		</fieldset>
		
		
		<div>
			<label for="date_of_birth">Date of birth</label>
			<input type="date" name="date_of_birth" required>
		</div>
		
		<div>
			<label for="type_of_work">Are you an employee or applicant?</label>
			<select id="employee_or_applicant" name="type_of_work">
				<option value="" selected="selected">Select One</option>
				<option value="employee">Employee</option>
				<option value="applicant">Applicant</option>
			</select>
		</div>
		
		<div>
			<label for="profile_description">Profile Description</label>
			<textarea name="description" maxlength="500"></textarea>
		</div>
		<button>Register</button>
	</form>
	
</body>
</html>
